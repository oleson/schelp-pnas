## Welcome! 

This repository contains the data and code associated with the manuscript "A transient dopamine signal encodes subjective value and causally influences demand in an economic context" by Schelp et al

### Contents

The repository contents are organized into the following sections (click/tap to navigate to each section):

- [Data](data/): Raw data
- [R](R/): R code for analysis of demand profiles
- [Results](results/): Parameter estimates


